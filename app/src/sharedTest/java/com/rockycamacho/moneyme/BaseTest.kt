package com.rockycamacho.moneyme

import androidx.test.core.app.ApplicationProvider
import com.jakewharton.threetenabp.AndroidThreeTen
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import timber.log.Timber

abstract class BaseTest {

    @Before
    @Throws(Exception::class)
    fun setUpLogging() {
        Timber.plant(TestTree())
    }

    @After
    @Throws(Exception::class)
    fun tearDownLogging() {
        Timber.uprootAll()
    }

}
