package com.rockycamacho.moneyme.endtoend

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.rockycamacho.moneyme.R

class SuccessRobot : BaseTestRobot() {

    fun isShown() {
        onView(withId(R.id.container)).check(matches(isDisplayed()))
        onView(withId(R.id.description)).check(matches(isDisplayed()))
        onView(withId(R.id.reset_button)).check(matches(isDisplayed()))
    }

    fun clickReset() {
        onView(withId(R.id.reset_button)).perform(click())
    }

    fun goToHome(func: HomeRobot.() -> Unit): HomeRobot {
        return home(func)
    }

}

fun success(func: SuccessRobot.() -> Unit) = SuccessRobot()
    .apply { func() }