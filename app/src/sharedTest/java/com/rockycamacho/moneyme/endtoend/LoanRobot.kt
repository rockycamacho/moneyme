package com.rockycamacho.moneyme.endtoend

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import com.rockycamacho.moneyme.R
import org.hamcrest.Matchers
import org.threeten.bp.LocalDate
import java.math.BigDecimal

class LoanRobot : BaseTestRobot() {

    fun isShown() {
        onView(withId(R.id.til_amount))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
//        onView(withId(R.id.amount_field))
//            .check(matches(isDisplayed()))
        onView(withId(R.id.til_duration))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
//        onView(withId(R.id.duration_field))
//            .check(matches(isDisplayed()))
        onView(withId(R.id.til_reasons))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.reasons_field))
            .check(matches(isDisplayed()))
        onView(withId(R.id.til_title))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
//        onView(withId(R.id.title_field)).check(matches(isDisplayed()))
        onView(withId(R.id.til_first_name))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.first_name_field))
            .check(matches(isDisplayed()))
        onView(withId(R.id.til_last_name))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.last_name_field))
            .check(matches(isDisplayed()))
        onView(withId(R.id.til_birthdate))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
//        onView(withId(R.id.birthdate_field))
//            .check(matches(isDisplayed()))
        onView(withId(R.id.til_email))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.email_field)).check(matches(isDisplayed()))
        onView(withId(R.id.til_mobile))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.mobile_field))
            .check(matches(isDisplayed()))
        onView(withId(R.id.submit_button))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
    }

    fun clickSubmit() {
        onView(withId(R.id.submit_button)).perform(scrollTo(), click())
    }

    fun goToSuccess(func: SuccessRobot.() -> Unit): SuccessRobot {
        return success(func)
    }

    fun fillForm(
        amount: BigDecimal? = null,
        duration: Int? = null,
        reasons: String? = null,
        title: String? = null,
        firstName: String? = null,
        lastName: String? = null,
        birthDate: LocalDate? = null,
        email: String? = null,
        mobile: String? = null
    ) {
        amount?.let {
            //TODO: possible issue with Robolectric and Dialogs
//            fillEditText(R.id.amount_field, amount.toString())
        }
        duration?.let {
            //TODO: possible issue with Robolectric and Dialogs
//            fillEditText(R.id.duration_field, duration.toString())
        }
        reasons?.let {
            fillEditText(R.id.reasons_field, reasons)
        }
        title?.let {
            when(title) {
                "Mr" -> clickButton(R.id.title_mr)
                "Mrs" -> clickButton(R.id.title_mrs)
                else -> clickButton(R.id.title_ms)
            }
        }
        firstName?.let {
            fillEditText(R.id.first_name_field, firstName)
        }
        lastName?.let {
            fillEditText(R.id.last_name_field, lastName)
        }
        birthDate?.let {
            //TODO: possible issue with Robolectric and Dialogs
//            clickButton(R.id.til_birthdate)
//            onView(withClassName(Matchers.equalTo(DatePickerDialog::class.java.name)))
//                .inRoot(isDialog())
//                .perform(PickerActions.setDate(birthDate.year, birthDate.monthValue, birthDate.dayOfMonth))
//            fillEditText(R.id.til_birthdate, birthDate)
        }
        email?.let {
            fillEditText(R.id.email_field, email)
        }
        mobile?.let {
            fillEditText(R.id.mobile_field, mobile)
        }
    }

}

fun loan(func: LoanRobot.() -> Unit) = LoanRobot()
    .apply { func() }