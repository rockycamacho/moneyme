package com.rockycamacho.moneyme.endtoend

import android.view.View
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import com.google.android.material.textfield.TextInputLayout
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher


fun hasTextInputLayoutErrorText(expectedErrorText: String): Matcher<View> {
    return object : TypeSafeMatcher<View>() {
        override fun describeTo(description: Description) {

        }

        override fun matchesSafely(view: View): Boolean {
            if (view !is TextInputLayout) {
                return false
            }

            val error = (view as TextInputLayout).error ?: return false

            val hint = error.toString()

            return expectedErrorText == hint
        }
    }
}

fun setProgress(progress: Int): ViewAction {
    return object : ViewAction {
        override fun getDescription(): String {
            return "Set a progress"
        }

        override fun getConstraints(): Matcher<View> {
            return ViewMatchers.isAssignableFrom(AppCompatSeekBar::class.java)
        }

        override fun perform(uiController: UiController, view: View) {
            (view as AppCompatSeekBar).setProgress(progress)
        }
    }
}