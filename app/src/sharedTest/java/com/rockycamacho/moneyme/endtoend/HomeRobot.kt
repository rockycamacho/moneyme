package com.rockycamacho.moneyme.endtoend

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.rockycamacho.moneyme.R

class HomeRobot : BaseTestRobot() {

    fun isShown() {
        onView(withId(R.id.container)).check(matches(isDisplayed()))
        onView(withId(R.id.amount)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_how_much))
            .check(matches(isDisplayed()))
        onView(withId(R.id.lbl_min_amount))
            .check(matches(isDisplayed()))
        onView(withId(R.id.lbl_max_amount))
            .check(matches(isDisplayed()))
        onView(withId(R.id.lbl_how_long))
            .check(matches(isDisplayed()))
        onView(withId(R.id.lbl_min_duration))
            .check(matches(isDisplayed()))
        onView(withId(R.id.lbl_max_duration))
            .check(matches(isDisplayed()))

        onView(withId(R.id.lbl_login_button))
            .check(matches(isDisplayed()))
        onView(withId(R.id.login_button))
            .check(matches(isDisplayed()))
        onView(withId(R.id.lbl_loan_button))
            .check(matches(isDisplayed()))
        onView(withId(R.id.loan_button)).check(matches(isDisplayed()))
    }

    fun clickLogin() {
        onView(withId(R.id.login_button)).perform(click())
    }

    fun goToLogin(func: BasicLoginRobot.() -> Unit): BasicLoginRobot {
        return login(func)
    }

    fun clickLoan() {
        onView(withId(R.id.loan_button)).perform(click())
    }

    fun goToLoan(func: LoanRobot.() -> Unit): LoanRobot {
        return loan(func)
    }

}

fun home(func: HomeRobot.() -> Unit) = HomeRobot()
    .apply { func() }