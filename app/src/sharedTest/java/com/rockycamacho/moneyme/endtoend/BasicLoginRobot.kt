package com.rockycamacho.moneyme.endtoend

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.rockycamacho.moneyme.R

class BasicLoginRobot : BaseTestRobot() {

    fun isShown() {
        onView(withId(R.id.til_email))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.email_field)).check(matches(isDisplayed()))
        onView(withId(R.id.til_password))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        onView(withId(R.id.password_field)).check(matches(isDisplayed()))
        onView(withId(R.id.login_button))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
    }

    fun clickSubmit() {
        onView(withId(R.id.login_button)).perform(scrollTo(), click())
    }

    fun fillForm(
        email: String? = null,
        password: String? = null
    ) {
        email?.let {
            fillEditText(R.id.email_field, email)
        }
        password?.let {
            fillEditText(R.id.password_field, password)
        }
    }

    fun goToLoan(func: LoanRobot.() -> Unit): LoanRobot {
        return loan(func)
    }

    fun goToHome(func: HomeRobot.() -> Unit): HomeRobot {
        return home(func)
    }

}

fun login(func: BasicLoginRobot.() -> Unit) = BasicLoginRobot()
    .apply { func() }