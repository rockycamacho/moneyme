package com.rockycamacho.moneyme.endtoend

import com.rockycamacho.moneyme.BaseRobolectricTest
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.TestComponent
import com.rockycamacho.moneyme.presentation.login.AuthProvider
import org.junit.Test
import org.mockito.Mock
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class HomeTest : BaseRobolectricTest() {
    @Mock
    lateinit var authProvider: AuthProvider
    @Inject
    lateinit var dateTimeFormatter: DateTimeFormatter

    override fun injectDependencies(testComponent: TestComponent) {
        testComponent.inject(this)
    }

    @Test
    fun `Required view components are present`() {
        home {
            isShown()
            matchText(R.id.lbl_min_amount, "2,100 AUD")
            matchText(R.id.lbl_max_amount, "15,000 AUD")
            matchText(R.id.lbl_min_duration, "3 months")
            matchText(R.id.lbl_max_duration, "36 months")
        }
    }

    @Test
    fun `Clicking the Member login button should prompt the user for their credentials before proceeding to the next screen`() {
        home {
            clickLogin()
        }.goToLogin {

        }
    }

    @Test
    fun `Clicking the Start Application button should redirect the user straight to the next screen`() {
        home {
            clickLoan()
        }.goToLoan {
            isShown()
        }
    }

}