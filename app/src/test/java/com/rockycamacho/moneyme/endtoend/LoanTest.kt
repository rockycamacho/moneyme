package com.rockycamacho.moneyme.endtoend

import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers
import com.rockycamacho.moneyme.BaseRobolectricTest
import com.rockycamacho.moneyme.MoneyMeApp
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.TestComponent
import com.rockycamacho.moneyme.presentation.home.HomeFragment
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.math.BigDecimal
import java.text.NumberFormat
import javax.inject.Inject

class LoanTest : BaseRobolectricTest() {

    @Inject
    lateinit var dateTimeFormatter: DateTimeFormatter
    @Inject
    lateinit var numberFormat: NumberFormat

    override fun injectDependencies(testComponent: TestComponent) {
        testComponent.inject(this)
    }

    @Test
    fun `Required view components are present`() {
        home {
            clickLoan()
        }
        loan {
            isShown()
        }
    }

    @Test
    fun `Given fields are all filled up, When submit button is clicked, Then app should save the loan details and show a success page`() {
        val minAmount =
            ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.min_loan_amount)
        val maxAmount =
            ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.max_loan_amount)
        val minDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
            .resources.getInteger(R.integer.min_loan_duration)
        val maxDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
            .resources.getInteger(R.integer.max_loan_duration)
        val amount = BigDecimal(faker.number().numberBetween(minAmount, maxAmount) / HomeFragment.AMOUNT_STEP * HomeFragment.AMOUNT_STEP)
        val duration = faker.number().numberBetween(minDuration, maxDuration)

        home {
            onView(ViewMatchers.withId(R.id.amount))
                .perform(setProgress(amount.toInt() - minAmount))
            onView(ViewMatchers.withId(R.id.duration))
                .perform(setProgress(duration - minDuration))

            clickLoan()
        }.goToLoan {
            matchText(R.id.til_amount, "${numberFormat.format(amount)} AUD")
            matchText(R.id.til_duration, "$duration months")

            fillForm(
                reasons = faker.lorem().sentence(5, 5),
                title = faker.options().option("Mr.", "Ms.", "Mrs."),
                firstName = faker.name().firstName(),
                lastName = faker.name().lastName(),
                birthDate = LocalDate.now(),
                email = faker.internet().safeEmailAddress(),
                mobile = faker.phoneNumber().cellPhone()
            )
            clickSubmit()
        }.goToSuccess {
            isShown()
        }
    }

    @Test
    fun `Given fields are not filled up, When submit button is clicked, app should show error messages`() {
        home {
            clickLoan()
        }.goToLoan {
            clickSubmit()
            isShown()
            hasFieldError(R.id.til_reasons, "This field is required")
//            hasFieldError(R.id.til_title, "This field is required")
            hasFieldError(R.id.til_first_name, "This field is required")
            hasFieldError(R.id.til_last_name, "This field is required")
            matchText(R.id.til_birthdate, "Select your birth date")
//            hasFieldError(R.id.til_birthdate, "This field is required")
            hasFieldError(R.id.til_email, "This field is required")
            hasFieldError(R.id.til_mobile, "This field is required")
        }
    }

}