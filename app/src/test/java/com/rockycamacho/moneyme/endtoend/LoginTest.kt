package com.rockycamacho.moneyme.endtoend

import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.rockycamacho.moneyme.BaseRobolectricTest
import com.rockycamacho.moneyme.MoneyMeApp
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.data.local.models.User
import com.rockycamacho.moneyme.di.TestComponent
import com.rockycamacho.moneyme.presentation.home.HomeFragment
import com.rockycamacho.moneyme.presentation.login.AuthProvider
import org.junit.Ignore
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import java.math.BigDecimal
import java.text.NumberFormat
import javax.inject.Inject

class LoginTest : BaseRobolectricTest() {
    @Inject
    lateinit var authProvider: AuthProvider
    @Inject
    lateinit var dateTimeFormatter: DateTimeFormatter
    @Inject
    lateinit var numberFormat: NumberFormat

    override fun injectDependencies(testComponent: TestComponent) {
        testComponent.inject(this)
    }

    @Ignore("Robolectric issue with RxJava. More investigation is needed")
    @Test
    fun `Clicking the Member login button should prompt the user for their credentials before proceeding to the next screen`() {
        val user =
            User(
                "1234-5678",
                "Mr",
                "Robert",
                "Baratheon",
                "robertbaratheon@example.com",
                LocalDate.of(1990, 6, 25)
            )
        val minAmount =
            ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.min_loan_amount)
        val maxAmount =
            ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.max_loan_amount)
        val minDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
            .resources.getInteger(R.integer.min_loan_duration)
        val maxDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
            .resources.getInteger(R.integer.max_loan_duration)
        val amount = BigDecimal(
            faker.number().numberBetween(
                minAmount,
                maxAmount
            ) / HomeFragment.AMOUNT_STEP * HomeFragment.AMOUNT_STEP
        )
        val duration = faker.number().numberBetween(minDuration, maxDuration)

        home {
            onView(withId(R.id.amount))
                .perform(setProgress(amount.toInt() - minAmount))
            onView(withId(R.id.duration))
                .perform(setProgress(duration - minDuration))

            clickLogin()
        }.goToLogin {
            val user = authProvider.login().blockingGet()
            testSchedulerRule.triggerActions()
        }.goToHome {
            isShown()
        }.goToLoan {
            Thread.sleep(200)

            isShown()

            matchText(R.id.til_amount, "${numberFormat.format(amount)} AUD")
            matchText(R.id.til_duration, "$duration months")

            matchText(R.id.first_name_field, user.firstName)
            matchText(R.id.last_name_field, user.lastName)
            matchText(R.id.til_birthdate, dateTimeFormatter.format(user.birthdate))
//            matchText(R.id.birthdate_field, dateTimeFormatter.format(user.birthdate))
        }
    }
}