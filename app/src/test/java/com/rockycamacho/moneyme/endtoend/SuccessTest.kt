package com.rockycamacho.moneyme.endtoend

import androidx.test.core.app.ApplicationProvider
import com.rockycamacho.moneyme.BaseRobolectricTest
import com.rockycamacho.moneyme.MoneyMeApp
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.TestComponent
import com.rockycamacho.moneyme.presentation.home.HomeFragment
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.math.BigDecimal
import javax.inject.Inject

class SuccessTest : BaseRobolectricTest() {
    @Inject
    lateinit var dateTimeFormatter: DateTimeFormatter

    override fun injectDependencies(testComponent: TestComponent) {
        testComponent.inject(this)
    }

    @Test
    fun `Required view components are present`() {
        home {
            clickLoan()
        }.goToLoan {
            val minAmount =
                ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.min_loan_amount)
            val maxAmount =
                ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.max_loan_amount)
            val minDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
                .resources.getInteger(R.integer.min_loan_duration)
            val maxDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
                .resources.getInteger(R.integer.max_loan_duration)
            fillForm(
                amount = BigDecimal(faker.number().numberBetween(minAmount, maxAmount) / HomeFragment.AMOUNT_STEP * HomeFragment.AMOUNT_STEP),
                duration = faker.number().numberBetween(minDuration, maxDuration),
                reasons = faker.lorem().sentence(5, 5),
                title = faker.options().option("Mr.", "Ms.", "Mrs."),
                firstName = faker.name().firstName(),
                lastName = faker.name().lastName(),
                birthDate = LocalDate.now(),
                email = faker.internet().safeEmailAddress(),
                mobile = faker.phoneNumber().cellPhone()
            )
            clickSubmit()
        }.goToSuccess {
            isShown()
        }
    }

    @Test
    fun `Clicking reset button should redirect user back to home page`() {
        home {
            clickLoan()
        }.goToLoan {
            val minAmount =
                ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.min_loan_amount)
            val maxAmount =
                ApplicationProvider.getApplicationContext<MoneyMeApp>().resources.getInteger(R.integer.max_loan_amount)
            val minDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
                .resources.getInteger(R.integer.min_loan_duration)
            val maxDuration = ApplicationProvider.getApplicationContext<MoneyMeApp>()
                .resources.getInteger(R.integer.max_loan_duration)
            fillForm(
                amount = BigDecimal(faker.number().numberBetween(minAmount, maxAmount) / HomeFragment.AMOUNT_STEP * HomeFragment.AMOUNT_STEP),
                duration = faker.number().numberBetween(minDuration, maxDuration),
                reasons = faker.lorem().sentence(5, 5),
                title = faker.options().option("Mr.", "Ms.", "Mrs."),
                firstName = faker.name().firstName(),
                lastName = faker.name().lastName(),
                birthDate = LocalDate.now(),
                email = faker.internet().safeEmailAddress(),
                mobile = faker.phoneNumber().cellPhone()
            )
            clickSubmit()
        }.goToSuccess {
            isShown()
            clickReset()
        }.goToHome {
            isShown()
        }
    }

}