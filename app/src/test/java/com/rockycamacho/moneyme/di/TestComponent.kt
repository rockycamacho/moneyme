package com.rockycamacho.moneyme.di

import com.rockycamacho.moneyme.LocalDateTest
import com.rockycamacho.moneyme.endtoend.HomeTest
import com.rockycamacho.moneyme.endtoend.LoanTest
import com.rockycamacho.moneyme.endtoend.LoginTest
import com.rockycamacho.moneyme.endtoend.SuccessTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        TestModule::class,
        DataModule::class
    ]
)
interface TestComponent {
    fun inject(test: HomeTest)
    fun inject(test: LoanTest)
    fun inject(test: SuccessTest)
    fun inject(test: LocalDateTest)
    fun inject(test: LoginTest)
}