package com.rockycamacho.moneyme.di

import androidx.fragment.app.FragmentActivity
import com.github.javafaker.Faker
import com.rockycamacho.moneyme.data.local.models.User
import com.rockycamacho.moneyme.presentation.login.AuthProvider
import dagger.Module
import dagger.Provides
import io.reactivex.Single
import org.threeten.bp.LocalDate
import javax.inject.Singleton

@OpenForTesting
@Module
class TestModule {

    @Singleton
    @Provides
    fun faker(): Faker = Faker.instance()

    @Singleton
    @Provides
    fun authProvider(): AuthProvider {
        return object : AuthProvider {
            override fun login(activity: FragmentActivity?): Single<User> {
                val user = User(
                    "1234-5678",
                    "Mr",
                    "Robert",
                    "Baratheon",
                    "robertbaratheon@example.com",
                    LocalDate.of(1990, 6, 25)
                )
                return Single.just(user)
            }

        }
    }

}
