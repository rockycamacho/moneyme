package com.rockycamacho.moneyme.domain

import com.rockycamacho.moneyme.BaseTest
import com.rockycamacho.moneyme.domain.loan.LoanCalculator
import junit.framework.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class LoanCalculatorTest: BaseTest() {

    @Test
    fun testMonthlyLoanComputation() {
        val loanCalculator = LoanCalculator()
        val pmt = loanCalculator.getPmt(BigDecimal(220000), 30 * 12)

        assertEquals("1147.62", pmt.toString())
    }
}