package com.rockycamacho.moneyme

import com.github.javafaker.Faker
import com.rockycamacho.moneyme.di.TestComponent
import org.junit.Rule
import javax.inject.Inject

abstract class BaseInjectedTest : BaseTest() {

    @Rule
    @JvmField
    var daggerRule = InjectedTestDaggerRule(this)
    @Rule
    @JvmField
    var testSchedulerRule = RxTestSchedulerRule()

    @Inject
    protected lateinit var faker: Faker

    abstract fun injectDependencies(testComponent: TestComponent)
}