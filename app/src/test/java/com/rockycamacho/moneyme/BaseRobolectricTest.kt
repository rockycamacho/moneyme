package com.rockycamacho.moneyme

import android.os.Build
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jakewharton.threetenabp.AndroidThreeTen
import com.rockycamacho.moneyme.di.TestComponent
import com.rockycamacho.moneyme.presentation.MainActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(
    sdk = [
        Build.VERSION_CODES.P,
        Build.VERSION_CODES.O,
        Build.VERSION_CODES.N,
        Build.VERSION_CODES.M
    ]
)
@RunWith(AndroidJUnit4::class)
abstract class BaseRobolectricTest : BaseInjectedTest() {
    protected lateinit var activityScenario: ActivityScenario<MainActivity>

    @Before
    fun setup() {
        AndroidThreeTen.init(ApplicationProvider.getApplicationContext())
        activityScenario = ActivityScenario.launch(MainActivity::class.java)
    }

}
