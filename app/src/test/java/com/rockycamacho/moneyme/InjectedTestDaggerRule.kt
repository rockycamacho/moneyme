package com.rockycamacho.moneyme

import com.rockycamacho.moneyme.di.DataModule
import com.rockycamacho.moneyme.di.TestComponent
import com.rockycamacho.moneyme.di.TestModule
import it.cosenonjaviste.daggermock.DaggerMockRule

class InjectedTestDaggerRule(test: BaseInjectedTest) : DaggerMockRule<TestComponent>(
    TestComponent::class.java,
    TestModule(),
    DataModule()
) {

    init {
        set { component ->
            test.injectDependencies(component)
        }
    }
}

