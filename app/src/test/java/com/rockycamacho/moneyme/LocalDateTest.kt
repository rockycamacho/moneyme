package com.rockycamacho.moneyme

import com.rockycamacho.moneyme.di.TestComponent
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber

class LocalDateTest: BaseRobolectricTest() {
    override fun injectDependencies(testComponent: TestComponent) {
        testComponent.inject(this)
    }

    @Test
    fun testFormat() {
        val now = LocalDate.of(2019, 1, 1)
        Timber.d("now: %s", now)
        val formatted = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(now)
        Timber.d("format: %s", formatted)
        assertEquals("2019-01-01", formatted)
    }

}