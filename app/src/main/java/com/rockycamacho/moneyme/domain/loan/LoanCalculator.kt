package com.rockycamacho.moneyme.domain.loan

import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

class LoanCalculator {

    companion object {
        val INTEREST_RATE_PER_YEAR = BigDecimal(0.0475)
        val INTEREST_RATE_PER_MONTH = INTEREST_RATE_PER_YEAR.divide(BigDecimal(12), RoundingMode.HALF_UP)
    }

    fun getPmt(
        loanAmount: BigDecimal,
        durationInMonths: Int,
        interestRatePerMonth: BigDecimal = INTEREST_RATE_PER_MONTH
    ): BigDecimal {
        return loanAmount.multiply(interestRatePerMonth)
            .divide(
                BigDecimal.ONE.subtract(
                    interestRatePerMonth.plus(
                        BigDecimal.ONE
                    ).pow(-durationInMonths, MathContext.DECIMAL64)
                ), RoundingMode.HALF_UP
            )
            .setScale(2, RoundingMode.HALF_UP)
    }

}