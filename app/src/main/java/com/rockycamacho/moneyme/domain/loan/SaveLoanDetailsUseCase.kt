package com.rockycamacho.moneyme.domain.loan

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDate
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

class SaveLoanDetailsUseCase @Inject constructor(){

    fun execute(
        loanAmount: Int,
        loanDuration: Int,
        loanReason: String,
        title: String,
        firstName: String,
        lastName: String,
        birthDate: LocalDate,
        email: String,
        mobile: String
    ): Single<Boolean> {
        //TODO: add actual implementation
        return Single.just(true)
            .delay(Random.nextLong(200, 500), TimeUnit.MILLISECONDS, Schedulers.io()) //fake delay to simulate network call
    }

}