package com.rockycamacho.moneyme.domain.login

import com.rockycamacho.moneyme.data.local.models.User
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDate
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

class LoginUseCase @Inject constructor() {
    fun execute(email: String, password: String): Single<User> {
        //TODO: add actual implementation
        return Single.fromCallable {
            User(
                "1234-5678",
                "Mr",
                "Robert",
                "Baratheon",
                "robertbaratheon@example.com",
                LocalDate.of(1990, 6, 25)
            )
        }.delay(
            Random.nextLong(200, 500),
            TimeUnit.MILLISECONDS,
            Schedulers.io()
        )    //fake delay to simulate network call
    }

}
