package com.rockycamacho.moneyme.data.local

import com.rockycamacho.moneyme.data.local.models.User

interface AppPreferences {

    fun amount(value: Int)
    fun amount(): Int
    fun duration(value: Int)
    fun duration(): Int

    fun user(value: User?)
    fun user(): User?

}