package com.rockycamacho.moneyme.data.local.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDate

@Parcelize
data class User(
    val id: String,
    val title: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val birthdate: LocalDate
) : Parcelable