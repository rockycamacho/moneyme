package com.rockycamacho.moneyme

import android.app.Application
import android.content.Context
import com.jakewharton.threetenabp.AndroidThreeTen
import com.rockycamacho.moneyme.di.*
import timber.log.Timber

class MoneyMeApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .dataModule(DataModule())
            .firebaseModule(FirebaseModule())
            .authModule(AuthModule())
            .build()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    companion object {
        operator fun get(context: Context): MoneyMeApp =
            context.applicationContext as MoneyMeApp
    }

}