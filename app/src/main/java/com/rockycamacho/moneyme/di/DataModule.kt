package com.rockycamacho.moneyme.di

import com.rockycamacho.moneyme.domain.loan.LoanCalculator
import dagger.Module
import dagger.Provides
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import java.text.NumberFormat
import java.util.*
import javax.inject.Singleton

@OpenForTesting
@Module
class DataModule {

    @Singleton
    @Provides
    fun dateTimeFormatter(): DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)

    @Singleton
    @Provides
    fun currencyFormatter(locale: Locale): NumberFormat {
        return NumberFormat.getNumberInstance(locale).apply {
            maximumFractionDigits = 2
            minimumFractionDigits = 0
            isGroupingUsed = true
        }
    }

    @Singleton
    @Provides
    fun locale(): Locale {
        return Locale.forLanguageTag("en_AU") ?: Locale.US
    }

    @Singleton
    @Provides
    fun loanCalculator(): LoanCalculator {
        return LoanCalculator()
    }

}