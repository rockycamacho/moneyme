package com.rockycamacho.moneyme.di

import com.rockycamacho.moneyme.presentation.login.BasicAuthProvider
import com.rockycamacho.moneyme.presentation.login.AuthProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AuthModule {

    @Singleton
    @Provides
    fun authProvider(): AuthProvider =
        BasicAuthProvider()

}
