package com.rockycamacho.moneyme.di

import com.rockycamacho.moneyme.presentation.*
import com.rockycamacho.moneyme.presentation.home.HomeFragment
import com.rockycamacho.moneyme.presentation.loan.LoanFragment
import com.rockycamacho.moneyme.presentation.login.LoginActivity
import com.rockycamacho.moneyme.presentation.success.SuccessFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    FirebaseModule::class,
    DataModule::class,
    AuthModule::class
])
interface AppComponent {
    fun inject(fragment: HomeFragment)
    fun inject(fragment: LoanFragment)
    fun inject(fragment: SuccessFragment)
    fun inject(activity: MainActivity)
    fun inject(activity: LoginActivity)
}