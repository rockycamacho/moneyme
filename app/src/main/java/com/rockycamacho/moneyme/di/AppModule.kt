package com.rockycamacho.moneyme.di

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@OpenForTesting
@Module
class AppModule constructor(private val app: Application) {

    @Singleton
    @Provides
    fun app(): Application = app

}
