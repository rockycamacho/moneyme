package com.rockycamacho.moneyme.presentation.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rockycamacho.moneyme.MoneyMeApp
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.AppComponent

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val appComponent = MoneyMeApp[this].appComponent
        injectDependencies(appComponent)
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
    }

    abstract fun injectDependencies(appComponent: AppComponent)

}
