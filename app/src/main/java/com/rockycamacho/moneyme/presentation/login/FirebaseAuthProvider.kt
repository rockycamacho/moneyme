package com.rockycamacho.moneyme.presentation.login

import androidx.fragment.app.FragmentActivity
import com.google.firebase.auth.FirebaseAuth
import com.rockycamacho.moneyme.data.local.models.User
import io.reactivex.Single
import javax.inject.Inject

@Deprecated(message = "Not implemented")
class FirebaseAuthProvider @Inject constructor(val firebaseAuth: FirebaseAuth) :
    AuthProvider {

    override fun login(activity: FragmentActivity?): Single<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}