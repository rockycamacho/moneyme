package com.rockycamacho.moneyme.presentation.login

import androidx.fragment.app.FragmentActivity
import com.rockycamacho.moneyme.data.local.models.User
import io.reactivex.Single

interface AuthProvider {
    companion object {
        const val EXTRA_USER: String = "user"
    }

    fun login(activity: FragmentActivity? = null): Single<User>
}