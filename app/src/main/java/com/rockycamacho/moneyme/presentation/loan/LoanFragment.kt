package com.rockycamacho.moneyme.presentation.loan

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.scale
import androidx.core.text.toSpannable
import androidx.dynamicanimation.animation.FloatPropertyCompat
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import com.afollestad.vvalidator.form
import com.maltaisn.calcdialog.CalcDialog
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.data.local.models.User
import com.rockycamacho.moneyme.di.AppComponent
import com.rockycamacho.moneyme.domain.loan.LoanCalculator
import com.rockycamacho.moneyme.domain.loan.SaveLoanDetailsUseCase
import com.rockycamacho.moneyme.presentation.base.BaseFragment
import com.rockycamacho.moneyme.presentation.base.DatePickerDialogFragment
import com.rockycamacho.moneyme.presentation.success.SuccessFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_loan.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import java.math.BigDecimal
import java.text.NumberFormat
import javax.inject.Inject

class LoanFragment : BaseFragment(), CalcDialog.CalcDialogCallback {

    @Inject
    lateinit var numberFormat: NumberFormat
    @Inject
    lateinit var loanCalculator: LoanCalculator
    @Inject
    lateinit var dateTimeFormatter: DateTimeFormatter
    @Inject
    lateinit var saveLoanDetailsUseCase: SaveLoanDetailsUseCase

    private lateinit var repaymentValueSpring: SpringAnimation
    private var birthdateSelected: LocalDate? = null
    private var loanAmount = 0
    private var loanDuration = 0
    private var displayedRepaymentAmount = 0f

    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.activity_loan

    companion object {
        const val REQUEST_AMOUNT = 1001
        const val REQUEST_DURATION = 1002
        const val AMOUNT = "amount"
        const val DURATION = "duration"
        const val USER = "user"

        fun newInstance(amount: Int, duration: Int, user: User?): LoanFragment {
            val fragment = LoanFragment()
            fragment.arguments = Bundle().apply {
                putInt(AMOUNT, amount)
                putInt(DURATION, duration)
                putParcelable(USER, user)
            }
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        refresh.isEnabled = false
        val spring = SpringForce(0f)
            .setDampingRatio(SpringForce.DAMPING_RATIO_NO_BOUNCY)
            .setStiffness(SpringForce.STIFFNESS_VERY_LOW)
        repaymentValueSpring =
            SpringAnimation(repayment_amount, object : FloatPropertyCompat<TextView>("repaymentAmount") {
                override fun getValue(textView: TextView): Float {
                    return displayedRepaymentAmount
                }

                override fun setValue(textView: TextView, value: Float) {
                    displayedRepaymentAmount = value
                    repayment_amount.text = SpannableStringBuilder()
                        .append("${numberFormat.format(displayedRepaymentAmount)}")
                        .scale(0.7f) { append(" AUD / Month") }
                        .toSpannable()
                }
            }).setSpring(spring)
                .setMinValue(0f)
                .setStartValue(0f)
                .setStartVelocity(0f)

        loanAmount = arguments!!.getInt(AMOUNT)
        loanDuration = arguments!!.getInt(DURATION)
        val user = arguments!!.getParcelable<User>(USER)

        til_amount.text = SpannableStringBuilder()
            .append("${numberFormat.format(loanAmount)}")
            .scale(0.7f) { append(" AUD") }
            .toSpannable()
        til_amount.setOnClickListener {
            val minAmount = requireActivity().resources.getInteger(R.integer.min_loan_amount)
            val maxAmount = requireActivity().resources.getInteger(R.integer.max_loan_amount)

            val dialog = CalcDialog()
            dialog.settings.requestCode = REQUEST_AMOUNT
            dialog.settings.initialValue = BigDecimal(loanAmount)
            dialog.settings.isSignBtnShown = false
            dialog.settings.minValue = BigDecimal(minAmount)
            dialog.settings.maxValue = BigDecimal(maxAmount)
            dialog.show(this@LoanFragment.childFragmentManager, "AmountCalculator")
        }

        til_duration.text = requireActivity().resources.getQuantityString(R.plurals.months, loanDuration, loanDuration)
        til_duration.setOnClickListener {
            val minDuration = requireActivity().resources.getInteger(R.integer.min_loan_duration)
            val maxDuration = requireActivity().resources.getInteger(R.integer.max_loan_duration)

            val dialog = CalcDialog()
            dialog.settings.requestCode = REQUEST_DURATION
            dialog.settings.initialValue = BigDecimal(loanDuration)
            dialog.settings.isSignBtnShown = false
            dialog.settings.minValue = BigDecimal(minDuration)
            dialog.settings.maxValue = BigDecimal(maxDuration)
            dialog.show(this@LoanFragment.childFragmentManager, "DurationCalculator")
        }

        val titleResId = when (user?.title) {
            "Mr" -> R.id.title_mr
            "Mrs" -> R.id.title_mrs
            "Ms" -> R.id.title_ms
            else -> R.id.title_mr
        }
        til_title.check(titleResId)

        til_birthdate.setOnClickListener {
            val dialog =
                DatePickerDialogFragment.newInstance(
                    DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                        birthdateSelected = LocalDate.of(year, month, dayOfMonth)
                        til_birthdate.text = dateTimeFormatter.format(birthdateSelected)
                    })
            dialog.show(childFragmentManager, "Calendar")
        }

        user?.let {
            first_name_field.setText(user.firstName)
            last_name_field.setText(user.lastName)
            birthdateSelected = user.birthdate
            til_birthdate.text = dateTimeFormatter.format(birthdateSelected)
            email_field.setText(user.email)
        }

        initializeForm()

        val repaymentAmount = loanCalculator.getPmt(BigDecimal(loanAmount), loanDuration)
        repaymentValueSpring.animateToFinalPosition(repaymentAmount.toFloat())
    }

    private fun initializeForm() {
        form {
            inputLayout(til_reasons) {
                isNotEmpty().description(R.string.error_required_field)
            }
            inputLayout(til_first_name) {
                isNotEmpty().description(R.string.error_required_field)
            }
            inputLayout(til_last_name) {
                isNotEmpty().description(R.string.error_required_field)
            }
            inputLayout(til_email) {
                isNotEmpty().description(R.string.error_required_field)
                isEmail().description(R.string.error_invalid_email)
            }
            inputLayout(til_mobile) {
                isNotEmpty().description(R.string.error_required_field)
            }

            submitWith(submit_button) { result ->
                submitLoanDetails()
            }
        }
    }

    private fun submitLoanDetails() {
        requireFragmentManager().beginTransaction()
            .replace(
                R.id.fragment_container,
                SuccessFragment.newInstance()
            )
            .addToBackStack(SuccessFragment::class.java.simpleName)
            .commit()

//        if (birthdateSelected == null) {
//            Toast.makeText(requireContext(), "Select a birth date", Toast.LENGTH_SHORT).show()
//            return@submitLoanDetails
//        }
//        val titleSelected = when (til_title.checkedButtonId) {
//            R.id.title_mr -> "Mr"
//            R.id.title_mrs -> "Mrs"
//            R.id.title_ms -> "Ms"
//            else -> "Mr"
//        }

//        saveLoanDetailsUseCase.execute(
//            loanAmount,
//            loanDuration,
//            reasons_field.text.toString(),
//            titleSelected,
//            first_name_field.text.toString(),
//            last_name_field.text.toString(),
////            birthdateSelected!!,
//            LocalDate.now(),
//            email_field.text.toString(),
//            mobile_field.text.toString()
//        ).subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//
//            }, { e ->
//                Timber.e(e)
//            })
    }

    override fun onValueEntered(requestCode: Int, value: BigDecimal?) {
        if (requestCode == REQUEST_AMOUNT) {
            value?.let {
                loanAmount = value.toInt()
                til_amount.text = SpannableStringBuilder()
                    .append("${numberFormat.format(loanAmount)}")
                    .scale(0.7f) { append(" AUD") }
                    .toSpannable()
                val repaymentAmount = loanCalculator.getPmt(BigDecimal(loanAmount), loanDuration)
                repaymentValueSpring.animateToFinalPosition(repaymentAmount.toFloat())
            }
        }
        if (requestCode == REQUEST_DURATION) {
            value?.let {
                loanDuration = value.toInt()
                til_duration.text =
                    requireActivity().resources.getQuantityString(R.plurals.months, loanDuration, loanDuration)
                val repaymentAmount = loanCalculator.getPmt(BigDecimal(loanAmount), loanDuration)
                repaymentValueSpring.animateToFinalPosition(repaymentAmount.toFloat())
            }
        }
    }

}
