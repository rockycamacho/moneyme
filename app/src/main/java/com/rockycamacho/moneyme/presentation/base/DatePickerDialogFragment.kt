package com.rockycamacho.moneyme.presentation.base

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.rockycamacho.moneyme.R
import org.threeten.bp.LocalDate
import java.util.*


class DatePickerDialogFragment : DialogFragment() {

    private lateinit var listener: DatePickerDialog.OnDateSetListener

    companion object {
        fun newInstance(listener: DatePickerDialog.OnDateSetListener): DatePickerDialogFragment {
            val fragment = DatePickerDialogFragment()
            fragment.listener = listener
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val now = LocalDate.now()
        val dialog = DatePickerDialog(
            requireContext(),
            R.style.AppDatePicker,
            listener,
            now.year - 18,
            now.monthValue,
            now.dayOfMonth
        )
        dialog.datePicker.id = R.id.mtrl_picker_text_input_date
        dialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
        return dialog
    }

}