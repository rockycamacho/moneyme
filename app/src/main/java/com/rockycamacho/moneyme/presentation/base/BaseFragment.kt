package com.rockycamacho.moneyme.presentation.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rockycamacho.moneyme.MoneyMeApp
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.AppComponent

abstract class BaseFragment : Fragment() {

    abstract fun injectDependencies(appComponent: AppComponent)

    abstract fun getLayoutResId(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val appComponent = MoneyMeApp[context].appComponent
        injectDependencies(appComponent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutResId(), container, false)
    }

}
