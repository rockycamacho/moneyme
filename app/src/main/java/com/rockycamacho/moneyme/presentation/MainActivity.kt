package com.rockycamacho.moneyme.presentation

import android.content.Intent
import android.os.Bundle
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.AppComponent
import com.rockycamacho.moneyme.presentation.base.BaseActivity
import com.rockycamacho.moneyme.presentation.home.HomeFragment

class MainActivity : BaseActivity() {
    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.fragment_container,
                    HomeFragment.newInstance()
                )
                .addToBackStack(HomeFragment::class.java.simpleName)
                .commit()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }
}
