package com.rockycamacho.moneyme.presentation.success

import android.os.Bundle
import android.view.View
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.AppComponent
import com.rockycamacho.moneyme.presentation.home.HomeFragment
import com.rockycamacho.moneyme.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.activity_success.*

class SuccessFragment : BaseFragment() {
    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.activity_success

    companion object {
        fun newInstance(): SuccessFragment {
            return SuccessFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        reset_button.setOnClickListener {
            requireFragmentManager().popBackStack(HomeFragment::class.java.simpleName, 0)
        }
    }

}
