package com.rockycamacho.moneyme.presentation.home

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.SeekBar
import androidx.core.text.scale
import androidx.core.text.toSpannable
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.data.local.models.User
import com.rockycamacho.moneyme.di.AppComponent
import com.rockycamacho.moneyme.presentation.base.BaseFragment
import com.rockycamacho.moneyme.presentation.loan.LoanFragment
import com.rockycamacho.moneyme.presentation.login.AuthProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_home.*
import timber.log.Timber
import java.text.NumberFormat
import javax.inject.Inject

class HomeFragment : BaseFragment() {
    @Inject
    lateinit var numberFormat: NumberFormat
    @Inject
    lateinit var authProvider: AuthProvider

    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.activity_home

    companion object {

        const val AMOUNT_STEP = 100

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        refresh.isEnabled = false
        val minAmount = requireActivity().resources.getInteger(R.integer.min_loan_amount)
        val maxAmount = requireActivity().resources.getInteger(R.integer.max_loan_amount)
        val minDuration = requireActivity().resources.getInteger(R.integer.min_loan_duration)
        val maxDuration = requireActivity().resources.getInteger(R.integer.max_loan_duration)
        lbl_min_amount.text = SpannableStringBuilder()
            .append("${numberFormat.format(minAmount)}")
            .scale(0.7f) { append(" AUD") }
            .toSpannable()
        lbl_max_amount.text = SpannableStringBuilder()
            .append("${numberFormat.format(maxAmount)}")
            .scale(0.7f) { append(" AUD") }
            .toSpannable()
        lbl_min_duration.text =
            requireActivity().resources.getQuantityString(R.plurals.months, minDuration, minDuration)
        lbl_max_duration.text =
            requireActivity().resources.getQuantityString(R.plurals.months, maxDuration, maxDuration)
        amount.max = maxAmount - minAmount
        amount.incrementProgressBy(100)
        duration.max = maxDuration - minDuration

        val currentValue = amount.progress / AMOUNT_STEP * AMOUNT_STEP + minAmount
        lbl_amount.text = SpannableStringBuilder()
            .append("${numberFormat.format(currentValue)}")
            .scale(0.7f) { append(" AUD") }
            .toSpannable()
        lbl_duration.text = requireActivity().resources.getQuantityString(
            R.plurals.months,
            duration.progress + minDuration,
            duration.progress + minDuration
        )

        amount.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                val progressValue = progress / AMOUNT_STEP * AMOUNT_STEP + minAmount
                lbl_amount.text = SpannableStringBuilder()
                    .append("${numberFormat.format(progressValue)}")
                    .scale(0.7f) { append(" AUD") }
                    .toSpannable()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }

        })

        duration.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                lbl_duration.text = requireActivity().resources.getQuantityString(
                    R.plurals.months,
                    progress + minDuration,
                    progress + minDuration
                )
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }

        })

        login_button.setOnClickListener {
            Timber.d("Loginbutton click")
            authProvider.login(requireActivity())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Timber.d("Loginbutton clicked")
                }
                .doOnTerminate {
                    Timber.d("Loginbutton click finish")
                }
                .subscribe({ user ->
                    goToLoan(minAmount, minDuration, user)
                }, { e ->
                    Timber.e(e)
                })
        }

        loan_button.setOnClickListener {
            goToLoan(minAmount, minDuration)
        }
    }

    private fun goToLoan(minAmount: Int, minDuration: Int, user: User? = null) {
        val amountSelected = amount.progress / AMOUNT_STEP * AMOUNT_STEP + minAmount
        val durationSelected = duration.progress + minDuration
        requireFragmentManager().beginTransaction()
            .replace(
                R.id.fragment_container,
                LoanFragment.newInstance(amountSelected, durationSelected, user)
            )
            .addToBackStack(LoanFragment::class.java.simpleName)
            .commit()
    }

}
