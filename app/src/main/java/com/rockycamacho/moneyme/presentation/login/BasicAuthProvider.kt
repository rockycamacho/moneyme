package com.rockycamacho.moneyme.presentation.login

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.petarmarijanovic.rxactivityresult.RxActivityResult
import com.rockycamacho.moneyme.data.local.models.User
import com.rockycamacho.moneyme.domain.login.LoginFailedException
import io.reactivex.Single

class BasicAuthProvider : AuthProvider {

    override fun login(activity: FragmentActivity?): Single<User> {
        val intent = Intent(activity, LoginActivity::class.java)
        return RxActivityResult(activity!!).start(intent)
            .map<User> { activityResult ->
                if (!activityResult.isOk) {
                    throw LoginFailedException()
                }
                return@map activityResult.data?.extras?.getParcelable<User>(AuthProvider.EXTRA_USER)
            }

    }

}