package com.rockycamacho.moneyme.presentation.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.afollestad.vvalidator.form
import com.rockycamacho.moneyme.R
import com.rockycamacho.moneyme.di.AppComponent
import com.rockycamacho.moneyme.domain.login.LoginUseCase
import com.rockycamacho.moneyme.presentation.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import timber.log.Timber
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    @Inject
    lateinit var loginUseCase: LoginUseCase

    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        refresh.isEnabled = false

        initializeForm()
    }

    private fun initializeForm() {
        form {
            inputLayout(til_email) {
                isNotEmpty().description(R.string.error_required_field)
                isEmail().description(R.string.error_invalid_email)
            }
            inputLayout(til_password) {
                isNotEmpty().description(R.string.error_required_field)
            }
            submitWith(login_button) {
                //TODO: move logic to a presenter
                val email = email_field.text.toString()
                val password = password_field.text.toString()
                loginUseCase.execute(email, password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        refresh.isRefreshing = true
                    }
                    .doOnTerminate {
                        refresh.isRefreshing = false
                    }
                    .subscribe({ user ->
                        val data = Intent()
                        data.putExtra(AuthProvider.EXTRA_USER, user)
                        setResult(Activity.RESULT_OK, data)
                        finish()
                    }, { e ->
                        Timber.e(e)
                        Toast.makeText(this@LoginActivity, e.message, Toast.LENGTH_SHORT).show()
                    })
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

}